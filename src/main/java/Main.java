import entities.Adress;
import entities.Mobile;
import entities.UserDetails;
import entities.Vehicles;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        UserDetails user = new UserDetails();
        user.setUsername("myusername");
        UserDetails user2 = new UserDetails();
        user2.setUsername("suername2");
        UserDetails user3 = new UserDetails();
        user3.setUsername("suername3");
        UserDetails user4 = new UserDetails();
        user4.setUsername("suername4");
        Adress adress = new Adress();
        adress.setCity("Lublin");
        adress.setStreet("Lwowska");
        adress.setUserDetails(user);
        user.setAdress(adress);
        Mobile mobile1 = new Mobile();
        mobile1.setPhoneNumber("12345679");
        Mobile mobile2 = new Mobile();
        mobile2.setPhoneNumber("234234123");
        List<Mobile> phones = new ArrayList<>();
        phones.add(mobile1);
        phones.add(mobile2);
        user.setPhones(phones);
        mobile1.setUser(user);
        mobile2.setUser(user);
        Vehicles vehicle1 = new Vehicles();
        vehicle1.setVehicleName("vehicle1");
        Vehicles vehicle2 = new Vehicles();
        vehicle2.setVehicleName("vehicle2");
        Vehicles vehicle3 = new Vehicles();
        vehicle3.setVehicleName("vehicle3");
        Vehicles vehicle4 = new Vehicles();
        vehicle4.setVehicleName("vehicle4");

        List<UserDetails> listOfUsers1 = new ArrayList<>();
        listOfUsers1.add(user);
        listOfUsers1.add(user2);
        List<UserDetails> listOfUsers2 = new ArrayList<>();
        listOfUsers2.add(user3);
        listOfUsers2.add(user4);
        List<Vehicles> listOFVehicles1 = new ArrayList<>();
        listOFVehicles1.add(vehicle1);
        listOFVehicles1.add(vehicle2);
        List<Vehicles> listOfVehicles2 = new ArrayList<>();
        listOfVehicles2.add(vehicle3);
        listOfVehicles2.add(vehicle4);
//        user.setVehicles(listOFVehicles1);
//        user2.setVehicles(listOFVehicles1);
//        user3.setVehicles(listOfVehicles2);
//        user4.setVehicles(listOfVehicles2);
        vehicle1.setUsers(listOfUsers1);
        vehicle2.setUsers(listOfUsers1);
        vehicle3.setUsers(listOfUsers2);
        vehicle4.setUsers(listOfUsers2);



        final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        final Session session = sessionFactory.openSession();

        session.beginTransaction();
        session.save(user);
        session.save(user2);
        session.save(user3);
        session.save(user4);

        session.save(adress);
        session.save(mobile1);
        session.save(mobile2);

        session.save(vehicle1);
        session.save(vehicle2);
        session.save(vehicle3);
        session.save(vehicle4);

        System.out.println("ta lnijka kodu nic nowego nie wnosi");

        Query from_userDetails = session.createQuery("from UserDetails", UserDetails.class);
        List<UserDetails> resultList = from_userDetails.getResultList();
        resultList.forEach(userDetails -> System.out.println(userDetails.getUsername()));

        session.getTransaction().commit();


        session.refresh(adress);
        session.close();
        HibernateUtils.shutdownSessionFactory();
    }
}
