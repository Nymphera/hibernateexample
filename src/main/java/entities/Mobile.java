package entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Mobile {
    @Id
    @GeneratedValue
    private int id;
    @Column (name = "phone_number")
    private String phoneNumber;
    @ManyToOne
    private UserDetails user;
}
