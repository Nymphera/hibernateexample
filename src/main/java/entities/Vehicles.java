package entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Vehicles {
    @Id
    @GeneratedValue
    private int id;
    @Column (name = "nazwa_pojazdu")
    private String vehicleName;
    @ManyToMany
    @JoinTable(
            name = "pojazdy_dla_uztkownikow",
            joinColumns = {@JoinColumn(name = "id_vehicle")},
            inverseJoinColumns = {@JoinColumn(name = "id_person")}
    )
    private List<UserDetails> users;
}
