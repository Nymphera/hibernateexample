package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor
public class Adress {
    @Id
    @GeneratedValue
    private int id;
    @Column (name = "city")
    private String  city;
    @Column  (name = "street")
    private String street;
    @OneToOne (mappedBy = "adress")
    private UserDetails userDetails;

    @Override
    public String toString() {
        return "Adress{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                '}';
    }
}
