package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity

@NoArgsConstructor
public class UserDetails {
    @Id
    @GeneratedValue
    private int id;
    @Column (name = "username")
    private String username;
    @OneToOne
    // @JoinColumn (name = "user_id") //zmenia nazwę kolumny
    private Adress adress;
    @OneToMany (mappedBy = "user")
    private List<Mobile> phones;
    @ManyToMany (mappedBy = "users")
    private List<Vehicles> vehicles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public List<Mobile> getPhones() {
        return phones;
    }

    public void setPhones(List<Mobile> phones) {
        this.phones = phones;
    }

    public List<Vehicles> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicles> vehicles) {
        this.vehicles = vehicles;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "id=" + id +
                ", username='" + username + '\'' +
                '}';
    }
}
